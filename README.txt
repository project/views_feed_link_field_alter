INTRODUCTION
------------

Views Feed Link Field Alter extends the existing RssFields Row Plugin
and alters:
    * channel link
    * item link
    * item GUID link


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Views Feed Link Field Alter module as you would normally install
   a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Structure > Views, select the desired View.
    2. The display's format has to be set to RSS Feed and the content needs
       to be showed as fields.
    3. Edit the general settings for the fields and assign any desired domain
       in the 'Path custom options' fieldset.


MAINTAINERS
-----------

Supporting organization:

 * Axis Communications AB - https://www.drupal.org/axis-communications-ab
